<!--Author: Carolin Schulz-->
<!--ID: 22178163-->

<!DOCTYPE html>

<html lang="en-ie">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CarTek Stock</title>
        <link rel="stylesheet" href="../styles/menubar.css"/>
        <link rel="stylesheet" href="../styles/stock.css"/>
        <link rel="stylesheet" href="../styles/footer.css"/>
        <link rel="stylesheet" href="../styles/media_stock.css"/>
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500&display=swap" rel="stylesheet">
    </head>

    <body>
        <div class="menuBar">
            <div class="logo">
                <img id="carteklogo" src="../images/logo_whitebackground.jpg" alt="CarTek Logo" />
            </div>
            <div class="home">
                <p>
                    <a href="../index.html">Home</a>
                </p>
            </div>
            <div class="buy">
                <p>
                <a href="./stock.html" class="links">Buy</a>
                </p>
            </div>
            <div class="sell">
                <p>
                    <span class="links">Sell</span>
                </p>
            </div>
            <div class="aboutus">
                <p>
                    <a href="../Comapny/about_us.html">About us</a>
                </p>
            </div>
            <div class="contactus">
                <p>
                    <a href="../Comapny/Contact_US.html">Contact us</a>
                </p>
            </div>
            <div class="blankspace"></div>
            <div class="languages">
                <img id="ireland" src="../images/ireland.png" alt="Language selector: English (Ireland)"/>
            </div>
        </div>


        <!--Picture with slogan-->
        <div class="container">
            <div class="centered">
                <p>You are one step closer</br>to discovering your dream car</p>
            </div>
        </div>

        <!--Newsletter Ad-->
        <div class="newsletter_grid">
            <div class="news">
                <p>CarTek News</p>
            </div>
            <div class="ad">
                <p>Keep up to date on our latest news. With updates on our
                    company, the brands we work with and more.
                </p>
            </div>
            <div >
                <a href="../Comapny/news/index.html">
                    <button class="news_button">CarTek News</button>
                </a>
            </div>
        </div>
        
        <h1 class="stock_header">Browse our stock</h1>

        <!--Filter Bar-->
        <?php
            $host="localhost";
            $user="root";
            $pass="";
            $dbName="db_ET4132_group6";
            $conn = new mysqli($host, $user, $pass, $dbName);
            if($conn->connect_error > 0){
                die("Failed to connect to database");
            }
        ?>
        <form action="./stock.php" method="post">
            <div class="middle_align">
                <div class="filter_flex"> <!--nested flexbox-->
                    <div class="price">
                        Price &nbsp;
                        <p class="price_elem">  <!--I'm setting default value of price to 0 - 90000-->
                            <input class="price_spectrum" type="text" name="min_price" maxlength="6" size="5px" value="0"/>
                            &#8364;&nbsp;-&nbsp;
                            <input class="price_spectrum" type="text" name="max_price" maxlength="6" size="5px" value="90000"/>
                            &#8364;
                        </p>
                    </div>
                    <div>
                        <select class="motor" name="engine_type">
                            <option value = "">Engine</option>
                            <?php
                                $sql = "SELECT * FROM engine";

                                if(!($result = $conn->query($sql))){
                                    die("Failed to process your request<br />");
                                }

                                while($row = $result->fetch_assoc()){
                                    $id = $row["engid"];
                                    $type = $row["type"];
                                    echo "<option value=\"$id\">$type</option>";
                                }
                                $result -> free();
                            ?>
                        </select>
                    </div>
                    <div>
                        <select class="gearbox" name="transmission">
                            <option value="">Gearbox</option>
                            <?php
                                $sql = "SELECT * FROM gearbox";
                                $result= $conn->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
                                while($row = $result->fetch_assoc()){
                                    $id = $row["gearid"];
                                    $name = $row["type"];
                                    echo "<option value=\"$id\">$name</option>";
                                }
                                $result->free();
                            ?>
                        </select>
                    </div>
                    <div>
                        <select class="make" name="brand">
                            <option value="">Make</option>
                            <?php
                                $sql = "SELECT * FROM makes";
								$result= $conn->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
                            while($row = $result->fetch_assoc()){
                                $id = $row["makeid"];
                                $name = $row["name"];
                                echo "<option value=\"$id\">$name</option>";
                            }
                            $result->free();
                            ?>
                        </select>
                    </div>
                    <div>
                        <select class="model" name="model">
                            <option value="">Model</option>
                            <?php
                                if(isset($_POST["brand"])){
                                    $make = $_POST["brand"];
                                    $sql = "SELECT model_id, model_name FROM model WHERE make_id = '$make'";
                                } else {
                                    $sql = "SELECT model_id, model_name FROM model";
                                }
								$result = $conn->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
								while($row = $result->fetch_assoc()){
									$id = $row["model_id"];
									$name = $row["model_name"];
									echo "<option value=\"$id\">$name</option>";
								}
                            $result->free();
                            ?>
                        </select>
                    </div>
                    <div>
                        <button type="submit" class="apply_button">Apply</button>
                    </div>
                </div>
            </div>
            

    <!--Horizontal rule-->

            <div class="match_flex">
                <?php
                if (isset($_POST["engine_type"])){

                    $query = $_POST["engine_type"];
                    $sql = "SELECT COUNT(*) as total FROM cars WHERE engineid = '$query'";

                    $result = $conn->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
					
                    $data = $result->fetch_assoc();
                    $count = $data["total"];
                    echo "<h3 class=\"match_count\"> $count matches</h3>";

                } else {

                    if (isset($_POST["transmission"])) {
                        $query = $_POST["transmission"];
                        $sql = "SELECT COUNT(*) AS total FROM cars WHERE transmission = '$query'";

                        $result->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
								
                        $data = $result->fetch_assoc();
                        $count = $data["total"];
                        echo "<h3 class=\"match_count\"> $count matches</h3>";

                    } else {

                        $sql = "SELECT COUNT(*) AS total FROM cars";

                        $result = $conn->query($sql);
                                if(!$result){
                                    die("Failed to process your request<br />");
                                }
						
                        $data = $result->fetch_assoc();
                        $count = $data["total"];
                        echo "<h3 class=\"match_count\"> $count matches</h3>";
                    }
                }
                $result -> free();
                ?>

                <div class="match_order_content">
                    <p id="sort" style="display: inline; font-size: 20px;color: grey;">Sort :&nbsp;&nbsp;</p>
                    <select class="match_order" name="match_order">
                        <option>Favourites</option>
                        <option>Price ascending</option>
                        <option>Price descending</option>
                    </select>
                </div>
            </div>
        </form>
    <hr/>

	<!--PHP: When no matches found, display: "Sorry, no matches found" centered in default font weight below hr-->
	
	<div class="stock-grid">
	<?php 
/*
	$make =  $_POST["brand"];
	$model = $_POST["model"];
	$engine = $_POST["engine_type"];
	$gearbox = $_POST["transmission"];
	$minprice = $_POST["min_price"];
	$maxprice = $_POST["max_price"];

	if (empty($make)) {
		$make = "%";
	}
	if (empty($model)) {
		$model = "%";
	}
	if (empty($engine)) {
		$enginee = "%";
	}
	if (empty($gearbox)) {
		$gearbox = "%";
	}
	if (empty($minprice)) {
		$make = "%";
	}
	if (empty($maxprice)) {
		$make = "%";
	}
*/
    if(isset($_POST["brand"])){
        $make = $_POST["brand"];
    } else {
        $make = "%";
    }
    if(isset($_POST["model"])){
        $model = $_POST["model"];
    } else {
        $model = "%";
    }
    if(isset($_POST["engine_type"])){
        $engine = $_POST["engine_type"];
    } else {
        $engine = "%";
    }
    if(isset($_POST["transmission"])){
        $gearbox = $_POST["transmission"];
    } else {
        $gearbox = "%";
    }
    $minprice = 0;
    $maxprice = 10000000000000000000;
	$sql = <<<EOD
		SELECT carid, m.model_name AS model, ma.name AS make, e.type AS engine, g.type AS gearbox, price, prevown FROM cars c
		JOIN model m ON (c.modelid = m.model_id)
		JOIN makes ma ON (m.make_id = ma.makeid)
		JOIN engine e ON (c.engineid = e.engid)
		JOIN gearbox g ON (c.transmission = g.gearid)

		WHERE ma.name LIKE "$make" AND m.model_name LIKE "$model" AND e.type LIKE "$engine" AND g.type LIKE "$gearbox" AND price >= $minprice AND price <= $maxprice;
EOD;

	$result = $conn ->query($sql);
	if (!$result){
		die("Failed to process your request<br />");
	}

	while ($row = $result->fetch_assoc()) {
		$carid = $row["carid"];
		$model = $row["model"];
		$make = $row["make"];
		$engine = $row["engine"];
		$gearbox = $row["gearbox"];
		$price = $row["price"];
		$prevown = $row["prevown"];

		// hardcoded values will be replaced with database values in a future version :)
		switch ($carid) {
            case 1:
                $img = "../images/stock_images/dima-panyukov-CoQIjfFg3cs-unsplash.jpg";
                $feature1 = "H + R Camera";
                $feature2 = "Navigation";
                $feature3 = "LED Headlights";
                break;
            case 2:
                $img = "../images/stock_images/287383_Volvo_C40_Recharge_Fjord_Blue.jpg";
                $feature1 = "Heated Seats";
                $feature2 = "Navigation";
                $feature3 = "Reversing Camera";
                break;
            case 3:
                $img = "../images/stock_images/alexander-ZIyIy8RFoQA-unsplash.jpg";
                $feature1 = "Reversing Sensor";
                $feature2 = "Bluetooth";
                $feature3 = "Reversing Camera";
                break;
            case 4:
                $img = "../images/stock_images/pexels-jarne-aerts-9050493.jpg";
                $feature1 = "Reversing Camera";
                $feature2 = "Navigation";
                $feature3 = "F + R Parking Sensors";
                break;
            case 5:
                $img = "../images/stock_images/vignesh-rajendran-36YnOKyTPEk-unsplash.jpg";
                $feature1 = "Speed Control";
                $feature2 = "Reversing Camerah";
                $feature3 = "Automatic Air Conditioning";
                break;
            case 6:
                $img = "../images/stock_images/alexander-ZIyIy8RFoQA-unsplash.jpg";
                $feature1 = "LED Headlights";
                $feature2 = "Speed Control";
                $feature3 = "Heated Seats";
                break;
            case 7:
                $img = "../images/stock_images/vincent-guzman-bSxT99_cqlY-unsplash.jpg";
                $feature1 = "Bluetooth";
                $feature2 = "Reversing Sensor";
                $feature3 = "Navigation";
                break;
            case 8:
                $img = "../images/stock_images/martin-katler-IBY1j7965pk-unsplash.jpg";
                $feature1 = "Reversing Sensor";
                $feature2 = "Blue";
                $feature3 = "Reversing Camera";
                break;
            case 9:
                $img = "../images/stock_images/renault.jpg";
                $feature1 = "Speed Control";
                $feature2 = "LED Headlights";
                $feature3 = "Reversing Sensor";
                break;
		}

		echo <<<EOD
		<div class="cars">
			<div class="car-img">
				<img class="image" src="$img" alt="$make $model"/>
			</div>
			<div class="car_info">
				<div class="car-name">
					$make $model
				</div>
			
				<div class="general_info">
					<b>Transmission:</b> &nbsp; <span class="car_transmission">$gearbox</span> </br>
					<b>Motor:</b> &nbsp; <span class="car_transmission">$engine</span> </br>
					<b>Previous Owners:</b> &nbsp; <span class="car_transmission">$prevown</span> </br>	
				</div>
	
				<!--Nested flexbox in grid-->
				<div class="car-features">
					<div class="car-features-flex">
						<div class="feature" id="feature1">
							$feature1
						</div>
						<div class="feature" id="feature2">
							$feature2
						</div>
					</div>
					<div class="car-features-flex">
						<div class="feature" id="feature3">
							$feature3
						</div>
						<div class="feature">& more</div> 	<!--Always show "& more" when more than 3 features-->
					</div>
				</div>
			</div>
		</div>
EOD;

	}
	$result -> free();
	?>
	</div>
	

	 <!--Book a Service Ad--> 
	 <a href="../career.html">
		<div class="career_ad">
			<div class="centered2">
				<p style="font-size: 35px; margin-bottom: 5px; font-weight: bold;">Your future at CarTek</p>
				<p style="font-size: 22px; margin-top: 0;">Explore our job opportunities</p>
			</div>
		</div>
	</a>



    </body>

	<!------------------------------------------------->
    <footer>
		<div id="background">

			<div id="left">
				<h1 class="branch">Company</h1>
				<p class="footer_links">
				<a href="../Comapny/career.html" class="footer_links">Career</a><br />
					Documents & Policies <br />
					Report 2022 <br />
				<a href="../Comapny/news/index.html" class="footer_links">News & Events</a>
				</p>
			</div>

			<div id="center-left">
				<h1 class="branch">Services</h1>
				<p class="footer_links">
					Book a test drive <br />
					Workshop <br />
					Customer support
				</p>
			</div>

			<div id="center-right">
				<h1 class="branch">Contact</h1>
				<p>
					<div class="customer_support_mail">E-mail: <br /> Cartek.customersupport&#64;<br />gmail.com <br /></div>
					<div class="phone_number">
						Tel: +353 505 12345 <br />
					</div>
					<div class="address">
						5 Main Street,
						Roscrea, <br />
						Co. Tipperary, <br />
						Ireland
					</div>
				</p>
			</div>

			<div id="right">
				<p>
					<div class="socialmedia">
						<div class="facebook">
							<img src="../images/facebook.png" alt="facebook logo" height="70px" />
						</div>
						<div class="instagram">
							<img src="../images/instagram.png" alt="instagram logo" height="80px"/>
						</div>
						<div class="twitter">
							<img src="../images/twitter.png" alt="twitter logo" height="80px"/>
						</div>
					</div>
					<br />

					<div class="cartek_logo">
						<img src="../images/logo_black_text.JPG" alt="CarTek logo with name beside it" height="50px"/> <br />
					</div>

					<div class="project_specs">
						<div class="website_updates">
							2022 - 2022
						</div>

						ET4132 Project - University of Limerick, </br>
						Ireland <br />
						Carolin Schulz, Eoghan Conlon,</br>
						Nelson Nwakaihe, Yuan Quan
					</div>
				</p>
			</div>
		</div>

	</footer>
</html>

    