<!DOCTYPE html>
<html lang="en-ie">
  <head>
	  <meta charset="utf-8" />
	  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	  <link rel="stylesheet" href="./../styles/contactstyle.css" />
	  <link rel="stylesheet" href="./../styles/menubar.css" />
	  <link rel="stylesheet" href="./../styles/footer.css" />
	  <link rel="preconnect" href="https://fonts.googleapis.com" />
	  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;500&display=swap" rel="stylesheet" />
	  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
	  <title>Contact Us</title>
  </head>

<body>
<?php 
		$host ="localhost";
		$user = "root";
		$password="";
		$dbName = "db_ET4132_group6";

		$conn = new mysqli($host, $user, $password, $dbName);

		if ($conn->connect_errno>0) {
			die("Unable to connect to DBMS and/or DB.</br>");
		}
	?>
 <div class="menuBar">
			<div class="logo">
				<img id="carteklogo" src="../images/logo_whitebackground.jpg" alt="CarTek Logo" />
			</div>
			<div class="home">
				<p>
					<a href="../index.html">Home</a>
				</p>
			</div>
			<div class="buy">
				<p>
					<a href="../Shop/stock.php" class="links">Buy</a>
				</p>
			</div>
			<div class="sell">
				<p>
					<span class="links">Sell</span>
				</p>
			</div>
			<div class="aboutus">
				<p>
					<a href="./about_us.html">About us</a>
				</p>
			</div>
			<div class="contactus">
				<p>
					<a href="./Contact_US.php">Contact us</a>
				</p>
			</div>
			<div class="blankspace"></div>
			<div class="languages">
				<img id="ireland" src="../images/ireland.png" alt="Language selector: English (Ireland)"/>
			</div>
</div>

<div class="content">
	<div class="left">
		<h1 class="contact_header"> Contact Us</h1>
		<p id="description"> Please complete the form below and
		we will reach out to you as soon as possible.
		</p>

	<form action="Contact_US.php" method="post">
		<div class="form">
			<table>
				<tr>
					<td class="request"><p style="font-size: larger;"> First Name</p></td>
					<td><input class="text" type="text" name="fname"/></td>
				</tr>
				<tr>
					<td class="request"><p style="font-size: larger;"> Last Name</p></td>
					<td><input class="text" type="text" name="lname"/></td>
				</tr>
				<tr>
					<td class="request"><p style="font-size: larger;"> Email</p></td>
					<td><input class="text" type="text" name="email"/></td>
				</tr>
				<tr>
					<td><br /><br /></td>
				</tr>
				<tr>
					<td class="request"><p style="font-size: larger;">Your Message</p></td>
					<td class="area"><textarea class="textfield" name="message" cols="23" rows="7"></textarea></td>
				</tr>
			</table>
		</div>

	  <?php
		if (!empty($_POST["fname"]) && !empty($_POST["lname"]) && !empty($_POST["email"]) && !empty($_POST["message"])) {
			//create new variables to store user input
			$fname = $_POST["fname"];
			$lname = $_POST["lname"];
			$email = $_POST["email"];
			$msg = $_POST["message"];

			//Add variable values to db in contact table
			$sql = "INSERT INTO contact VALUES (DEFAULT, \"$fname\", \"$lname\", \"$email\", \"$msg\");";
			if (!$conn->query($sql)) {
				die("Unable to execute your SQL request");
			} else {
				echo "<p><br/>Your form was sent successfully</p>";
			}
			
		} else {
			echo "<p><br/>Please fill out every input field</p>";
		}
		$conn->close();	
	 ?>
	
		<p id="dperror"></p>
		<div class="buttons">
			<button class="submit-button" type="submit">Submit</button>
			<button class="reset-button" type="reset">Reset</button>
		</div>
	  </form>
	</div>

	<div class="right">
		<span><img id="page_image" src="../images/customer_service.JPG"></span>
	</div>
</div>
</body>

<!--FOOTER-->
<footer>
	<div id="background">

		<div id="left">
			<h1 class="branch">Company</h1>
			<p class="footer_links">
				<a href="./career.html" class="footer_links">Career</a><br />
					Documents & Policies <br />
					Report 2022 <br />
				<a href="./news/index.html" class="footer_links">News & Events</a>
			</p>
		</div>

		<div id="center-left">
			<h1 class="branch">Services</h1>
			<p class="footer_links">
				Book a test drive <br />
				Workshop <br />
				Customer support
			</p>
		</div>

		<div id="center-right">
			<h1 class="branch">Contact</h1>
			<p>
				<div class="customer_support_mail">E-mail: </br> Cartek.customersupport&#64;</br>gmail.com <br /></div>
				<div class="phone_number">
					Tel: +353 505 12345 <br />
				</div>
				<div class="address">
					5 Main Street,
					Roscrea, <br />
					Co. Tipperary, <br />
					Ireland
				</div>
			</p>
		</div>

		<div id="right">
			<p>
				<div class="socialmedia">
					<div class="facebook">
						<img src="../images/facebook.png" alt="facebook logo" height="70px" />
					</div>
					<div class="instagram">
						<img src="../images/instagram.png" alt="instagram logo" height="80px"/>
					</div>
					<div class="twitter">
						<img src="../images/twitter.png" alt="twitter logo" height="80px"/>
					</div>
				</div>
				<br />

				<div class="cartek_logo">
					<img src="../images/logo_black_text.JPG" alt="CarTek logo with name beside it" height="50px"/> <br />
				</div>

				<div class="project_specs">
					<div class="website_updates">
						2022 - 2022
					</div>

					ET4132 Project - University of Limerick, </br>
					Ireland <br />
					Carolin Schulz, Eoghan Conlon,</br>
					Nelson Nwakaihe, Yuan Quan
				</div>
			</p>
		</div>
	</div>

</footer>
 </html>

 <!--minor changes to be added (if time allows so):
	- menu bar ireland logo with dropdown menu
	- add footer (this is definitly necessary)
	- hover animation for data protection terms/change color when active or after
	- add javascript so that you have to agree to terms to be able to submit
-->